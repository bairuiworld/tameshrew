/* -*-c++-*- OpenSceneGraph - Copyright (C) 1998-2003 Robert Osfield 
 *
 * This library is open source and may be redistributed and/or modified under  
 * the terms of the OpenSceneGraph Public License (OSGPL) version 0.0 or 
 * (at your option) any later version.  The full license is in LICENSE file
 * included with this distribution, and on the openscenegraph.org website.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * OpenSceneGraph Public License for more details.
 */
#include "PatternDrawable.hpp"
#include <iostream>
using namespace std;

///////////////////////////////////////////////////////////////////////////////
//
// draw pattern
//

class DrawPatternVisitor : public ConstPatternVisitor
{
public:
    
    DrawPatternVisitor(State& state,const TessellationHints* hints):
        _state(state),
        _hints(hints)
    {
        if (hints) {
            notify(NOTICE)<<"Warning: TessellationHints ignored in present PatternDrawable implementation."<<std::endl;
        }
        
    }
    
    virtual void apply(const Circle&);
    virtual void apply(const Oblong&);
    virtual void apply(const Cornet&);
    virtual void apply(const Pipe&);

    State&                      _state;
    const TessellationHints*    _hints;
};

void DrawPatternVisitor::apply(const Circle& circle)
{
    glPushMatrix();

    glTranslatef(circle.getCenter().x(), circle.getCenter().y(), circle.getCenter().z());

    float startAngle = circle.getStartAngle();
    float endAngle = circle.getEndAngle();
    float texCoord = 0.0f;

    unsigned int numSegments = 40;

    float angleDelta = 2.0f * osg::PI / (float)numSegments;
    float widthAngle = circle.getWidth() * osg::PI / 2.0f;

    float texCoordDelta = 1.0 / (float)numSegments;

    float r = circle.getRadius();
    float w = circle.getRadius() * circle.getWidth();

    float r1 = r - w;

    // pipe body

    glBegin(GL_QUAD_STRIP);

    float angle = startAngle;
    for(unsigned int bodyi = 0;
        bodyi < numSegments;
        ++bodyi, angle += angleDelta, texCoord += texCoordDelta)
        {

            float c = cosf(angle);
            float s = sinf(angle);

            glNormal3f(c,s,0.0f);

            if (endAngle > 0.0 && angle >= endAngle) {
                bodyi = numSegments;
                angle = endAngle;
            }

            glTexCoord2f(texCoord,1.0f);
            glVertex3f(c*r,s*r,0.0f);

            glTexCoord2f(texCoord,0.0f);
            glVertex3f(c*r1,s*r1,0.0f);

        }

    // do last point by hand to ensure no round off errors.

    if (endAngle > 0.0) {
        angle = endAngle;
        float c = cosf(angle);
        float s = sinf(angle);

        glTexCoord2f(texCoord,1.0f);
        glVertex3f(c*r,s*r,0.0f);

        angle -= widthAngle;
        c = cosf(angle);
        s = sinf(angle);

        glTexCoord2f(texCoord,0.0f);
        glVertex3f(c*r,s*r,0.0f);

        angle = startAngle;
        c = cosf(angle);
        s = sinf(angle);

        glNormal3f(c,s,0.0f);

        glTexCoord2f(1.0f,1.0f);
        glVertex3f(c*r,s*r,0.0f);

        angle += widthAngle;
        c = cosf(angle);
        s = sinf(angle);

        glTexCoord2f(1.0f,0.0f);
        glVertex3f(c*r,s*r,0.0f);
    }
    else {
        glNormal3f(1.0f,0.0f,0.0f);

        glTexCoord2f(1.0f,1.0f);
        glVertex3f(r,0.0f,0.0f);

        glTexCoord2f(1.0f,0.0f);
        glVertex3f(r1,0.0f,0.0f);
    }
    
    glEnd();

    glPopMatrix();
}

void DrawPatternVisitor::apply(const Oblong& oblong)
{

    float dx = oblong.getHalfLengths().x();
    float dy = oblong.getHalfLengths().y();
    float dz = oblong.getHalfLengths().z();

    glPushMatrix();

    glTranslatef(oblong.getCenter().x(),oblong.getCenter().y(),oblong.getCenter().z());

    if (!oblong.zeroRotation()) {
        Matrix rotation(oblong.getRotationMatrix());
        glMultMatrixf((const GLfloat *)rotation.ptr());
    }

    glBegin(GL_QUADS);

    // -ve y plane
    glNormal3f(0.0f,-1.0f,0.0f);

    glTexCoord2f(0.0f,1.0f);
    glVertex3f(-dx,-dy,dz);

    glTexCoord2f(0.0f,0.0f);
    glVertex3f(-dx,-dy,-dz);

    glTexCoord2f(1.0f,0.0f);
    glVertex3f(dx,-dy,-dz);

    glTexCoord2f(1.0f,1.0f);
    glVertex3f(dx,-dy,dz);

    // +ve y plane
    glNormal3f(0.0f,1.0f,0.0f);

    glTexCoord2f(0.0f,1.0f);
    glVertex3f(dx,dy,dz);

    glTexCoord2f(0.0f,0.0f);
    glVertex3f(dx,dy,-dz);

    glTexCoord2f(1.0f,0.0f);
    glVertex3f(-dx,dy,-dz);

    glTexCoord2f(1.0f,1.0f);
    glVertex3f(-dx,dy,dz);

    // +ve x plane
    glNormal3f(1.0f,0.0f,0.0f);

    glTexCoord2f(0.0f,1.0f);
    glVertex3f(dx,-dy,dz);

    glTexCoord2f(0.0f,0.0f);
    glVertex3f(dx,-dy,-dz);

    glTexCoord2f(1.0f,0.0f);
    glVertex3f(dx,dy,-dz);

    glTexCoord2f(1.0f,1.0f);
    glVertex3f(dx,dy,dz);

    // -ve x plane
    glNormal3f(-1.0f,0.0f,0.0f);

    glTexCoord2f(0.0f,1.0f);
    glVertex3f(-dx,dy,dz);

    glTexCoord2f(0.0f,0.0f);
    glVertex3f(-dx,dy,-dz);

    glTexCoord2f(1.0f,0.0f);
    glVertex3f(-dx,-dy,-dz);

    glTexCoord2f(1.0f,1.0f);
    glVertex3f(-dx,-dy,dz);

    // +ve z plane
    glNormal3f(0.0f,0.0f,1.0f);

    glTexCoord2f(0.0f,1.0f);
    glVertex3f(-dx,dy,dz);

    glTexCoord2f(0.0f,0.0f);
    glVertex3f(-dx,-dy,dz);

    glTexCoord2f(1.0f,0.0f);
    glVertex3f(dx,-dy,dz);

    glTexCoord2f(1.0f,1.0f);
    glVertex3f(dx,dy,dz);

    // -ve z plane
    glNormal3f(0.0f,0.0f,-1.0f);

    glTexCoord2f(0.0f,1.0f);
    glVertex3f(dx,dy,-dz);

    glTexCoord2f(0.0f,0.0f);
    glVertex3f(dx,-dy,-dz);

    glTexCoord2f(1.0f,0.0f);
    glVertex3f(-dx,-dy,-dz);

    glTexCoord2f(1.0f,1.0f);
    glVertex3f(-dx,dy,-dz);

    glEnd();

    glPopMatrix();

}

void DrawPatternVisitor::apply(const Cornet& cornet)
{
    glPushMatrix();

    glTranslatef(cornet.getCenter().x(),cornet.getCenter().y(),cornet.getCenter().z());

    if (!cornet.zeroRotation()) {
        Matrix rotation(cornet.getRotationMatrix());
        glMultMatrixf((const GLfloat *)rotation.ptr());
    }


    unsigned int numSegments = 40;
    unsigned int numRows = 10;
    
    float r = cornet.getRadius();
    float h = cornet.getHeight();
    
    float normalz = r/(sqrtf(r*r+h*h));
    float normalRatio = 1.0f/(sqrtf(1.0f+normalz*normalz));
    normalz *= normalRatio;

    float angleDelta = 2.0f*osg::PI/(float)numSegments;
    float texCoordHorzDelta = 1.0/(float)numSegments;
    float texCoordRowDelta = 1.0/(float)numRows;
    float hDelta =  cornet.getHeight()/(float)numRows;
    float rDelta =  cornet.getRadius()/(float)numRows;

    float topz=cornet.getHeight()+cornet.getBaseOffset();
    float topr=0.0f;
    float topv=1.0f;
    float basez=topz-hDelta;
    float baser=rDelta;
    float basev=topv-texCoordRowDelta;
    float angle;
    float texCoord;

    for(unsigned int rowi=0;
        rowi<numRows;
        ++rowi,topz=basez, basez-=hDelta, topr=baser, baser+=rDelta, topv=basev, basev-=texCoordRowDelta)
        {
            // we can't use a fan for the cornet top
            // since we need different normals at the top
            // for each face..
            glBegin(GL_QUAD_STRIP);

            angle = 0.0f;
            texCoord = 0.0f;
            for(unsigned int topi=0;
                topi<numSegments;
                ++topi,angle+=angleDelta,texCoord+=texCoordHorzDelta)
                {

                    float c = cosf(angle);
                    float s = sinf(angle);

                    glNormal3f(c*normalRatio,s*normalRatio,normalz);

                    glTexCoord2f(texCoord,topv);
                    glVertex3f(c*topr,s*topr,topz);

                    glTexCoord2f(texCoord,basev);
                    glVertex3f(c*baser,s*baser,basez);

                }

            // do last point by hand to ensure no round off errors.
            glNormal3f(normalRatio,0.0f,normalz);

            glTexCoord2f(1.0f,topv);
            glVertex3f(topr,0.0f,topz);

            glTexCoord2f(1.0f,basev);
            glVertex3f(baser,0.0f,basez);

            glEnd();

        }

    // we can't use a fan for the cornet top
    // since we need different normals at the top
    // for each face..
    glBegin(GL_TRIANGLE_FAN);
    
    angle = osg::PI*2.0f;
    texCoord = 1.0f;
    basez = cornet.getBaseOffset();

    glNormal3f(0.0f,0.0f,-1.0f);
    glTexCoord2f(0.5f,0.5f);
    glVertex3f(0.0f,0.0f,basez);

    for(unsigned int bottomi=0;
        bottomi<numSegments;
        ++bottomi,angle-=angleDelta,texCoord-=texCoordHorzDelta)
        {

            float c = cosf(angle);
            float s = sinf(angle);

            glTexCoord2f(c*0.5f+0.5f,s*0.5f+0.5f);
            glVertex3f(c*r,s*r,basez);

        }

    glTexCoord2f(1.0f,0.0f);
    glVertex3f(r,0.0f,basez);
    
    glEnd();

    glPopMatrix();
}

void DrawPatternVisitor::apply(const Pipe& pipe)
{
    glPushMatrix();

    glTranslatef(pipe.getCenter().x(),pipe.getCenter().y(),pipe.getCenter().z());

    if (!pipe.zeroRotation()) {
        Matrix rotation(pipe.getRotationMatrix());
        glMultMatrixf((GLfloat *)rotation.ptr());
    }


    unsigned int numSegments = 40;
    
    float angleDelta = 2.0f*osg::PI/(float)numSegments;
    
    float texCoordDelta = 1.0/(float)numSegments;
    
    float r = pipe.getRadius();
    float h = pipe.getHeight();

    float basez = -h*0.5f;
    float topz = h*0.5f;

    // pipe body
    glBegin(GL_QUAD_STRIP);

    float angle = 0.0f;
    float texCoord = 0.0f;
    for(unsigned int bodyi=0;
        bodyi<numSegments;
        ++bodyi,angle+=angleDelta,texCoord+=texCoordDelta)
        {

            float c = cosf(angle);
            float s = sinf(angle);

            glNormal3f(c,s,0.0f);

            glTexCoord2f(texCoord,1.0f);
            glVertex3f(c*r,s*r,topz);

            glTexCoord2f(texCoord,0.0f);
            glVertex3f(c*r,s*r,basez);

        }

    // do last point by hand to ensure no round off errors.
    glNormal3f(1.0f,0.0f,0.0f);

    glTexCoord2f(1.0f,1.0f);
    glVertex3f(r,0.0f,topz);

    glTexCoord2f(1.0f,0.0f);
    glVertex3f(r,0.0f,basez);
    
    glEnd();
    /*
    // pipe top
    glBegin(GL_TRIANGLE_FAN);
    
    glNormal3f(0.0f,0.0f,1.0f);
    glTexCoord2f(0.5f,0.5f);
    glVertex3f(0.0f,0.0f,topz);

    angle = 0.0f;
    texCoord = 0.0f;
    for(unsigned int topi=0;
    topi<numSegments;
    ++topi,angle+=angleDelta,texCoord+=texCoordDelta)
    {

    float c = cosf(angle);
    float s = sinf(angle);

    glTexCoord2f(c*0.5f+0.5f,s*0.5f+0.5f);
    glVertex3f(c*r,s*r,topz);

    }

    glTexCoord2f(1.0f,0.0f);
    glVertex3f(r,0.0f,topz);
    
    glEnd();

    // pipe bottom
    glBegin(GL_TRIANGLE_FAN);
    
    glNormal3f(0.0f,0.0f,-1.0f);
    glTexCoord2f(0.5f,0.5f);
    glVertex3f(0.0f,0.0f,basez);

    angle = osg::PI*2.0f;
    texCoord = 1.0f;
    for(unsigned int bottomi=0;
    bottomi<numSegments;
    ++bottomi,angle-=angleDelta,texCoord-=texCoordDelta)
    {

    float c = cosf(angle);
    float s = sinf(angle);

    glTexCoord2f(c*0.5f+0.5f,s*0.5f+0.5f);
    glVertex3f(c*r,s*r,basez);

    }

    glTexCoord2f(0.0f,0.0f);
    glVertex3f(r,0.0f,basez);
    
    glEnd();
    */

    glPopMatrix();
}

///////////////////////////////////////////////////////////////////////////////
//
// Compute bounding of pattern
//

class ComputeBoundPatternVisitor : public ConstPatternVisitor
{
public:
    
    ComputeBoundPatternVisitor(BoundingBox& bb):_bb(bb) {}
    
    virtual void apply(const Circle&);
    virtual void apply(const Oblong&);
    virtual void apply(const Cornet&);
    virtual void apply(const Pipe&);

    BoundingBox&    _bb;
};


void ComputeBoundPatternVisitor::apply(const Circle& circle)
{
    Vec3 halfLengths(circle.getRadius(),circle.getRadius(),circle.getRadius());
    _bb.expandBy(circle.getCenter()-halfLengths);
    _bb.expandBy(circle.getCenter()+halfLengths);
}

void ComputeBoundPatternVisitor::apply(const Oblong& oblong)
{
    if (oblong.zeroRotation()) {
        _bb.expandBy(oblong.getCenter()-oblong.getHalfLengths());
        _bb.expandBy(oblong.getCenter()+oblong.getHalfLengths());
    }
    else {
        float x = oblong.getHalfLengths().x();
        float y = oblong.getHalfLengths().y();
        float z = oblong.getHalfLengths().z();

        Vec3 base_1(Vec3(-x,-y,-z));
        Vec3 base_2(Vec3(x,-y,-z));
        Vec3 base_3(Vec3(x,y,-z));
        Vec3 base_4(Vec3(-x,y,-z));
    
        Vec3 top_1(Vec3(-x,-y,z));
        Vec3 top_2(Vec3(x,-y,z));
        Vec3 top_3(Vec3(x,y,z));
        Vec3 top_4(Vec3(-x,y,z));

        Matrix matrix = oblong.getRotationMatrix();
        _bb.expandBy(oblong.getCenter()+base_1*matrix);
        _bb.expandBy(oblong.getCenter()+base_2*matrix);
        _bb.expandBy(oblong.getCenter()+base_3*matrix);
        _bb.expandBy(oblong.getCenter()+base_4*matrix);

        _bb.expandBy(oblong.getCenter()+top_1*matrix);
        _bb.expandBy(oblong.getCenter()+top_2*matrix);
        _bb.expandBy(oblong.getCenter()+top_3*matrix);
        _bb.expandBy(oblong.getCenter()+top_4*matrix);
    }
}

void ComputeBoundPatternVisitor::apply(const Cornet& cornet)
{
    if (cornet.zeroRotation()) {
        _bb.expandBy(cornet.getCenter()+Vec3(-cornet.getRadius(),-cornet.getRadius(),cornet.getBaseOffset()));
        _bb.expandBy(cornet.getCenter()+Vec3(cornet.getRadius(),cornet.getRadius(),cornet.getHeight()+cornet.getBaseOffset()));
    
    }
    else {
        Vec3 top(Vec3(cornet.getRadius(),cornet.getRadius(),cornet.getHeight()+cornet.getBaseOffset()));
        Vec3 base_1(Vec3(-cornet.getRadius(),-cornet.getRadius(),cornet.getBaseOffset()));
        Vec3 base_2(Vec3(cornet.getRadius(),-cornet.getRadius(),cornet.getBaseOffset()));
        Vec3 base_3(Vec3(cornet.getRadius(),cornet.getRadius(),cornet.getBaseOffset()));
        Vec3 base_4(Vec3(-cornet.getRadius(),cornet.getRadius(),cornet.getBaseOffset()));
    
        Matrix matrix = cornet.getRotationMatrix();
        _bb.expandBy(cornet.getCenter()+base_1*matrix);
        _bb.expandBy(cornet.getCenter()+base_2*matrix);
        _bb.expandBy(cornet.getCenter()+base_3*matrix);
        _bb.expandBy(cornet.getCenter()+base_4*matrix);
        _bb.expandBy(cornet.getCenter()+top*matrix);
    }
}

void ComputeBoundPatternVisitor::apply(const Pipe& pipe)
{
    if (pipe.zeroRotation()) {
        Vec3 halfLengths(pipe.getRadius(),pipe.getRadius(),pipe.getHeight()*0.5f);
        _bb.expandBy(pipe.getCenter()-halfLengths);
        _bb.expandBy(pipe.getCenter()+halfLengths);

    }
    else {
        float r = pipe.getRadius();
        float z = pipe.getHeight()*0.5f;

        Vec3 base_1(Vec3(-r,-r,-z));
        Vec3 base_2(Vec3(r,-r,-z));
        Vec3 base_3(Vec3(r,r,-z));
        Vec3 base_4(Vec3(-r,r,-z));
    
        Vec3 top_1(Vec3(-r,-r,z));
        Vec3 top_2(Vec3(r,-r,z));
        Vec3 top_3(Vec3(r,r,z));
        Vec3 top_4(Vec3(-r,r,z));

        Matrix matrix = pipe.getRotationMatrix();
        _bb.expandBy(pipe.getCenter()+base_1*matrix);
        _bb.expandBy(pipe.getCenter()+base_2*matrix);
        _bb.expandBy(pipe.getCenter()+base_3*matrix);
        _bb.expandBy(pipe.getCenter()+base_4*matrix);

        _bb.expandBy(pipe.getCenter()+top_1*matrix);
        _bb.expandBy(pipe.getCenter()+top_2*matrix);
        _bb.expandBy(pipe.getCenter()+top_3*matrix);
        _bb.expandBy(pipe.getCenter()+top_4*matrix);
    }
}

///////////////////////////////////////////////////////////////////////////////
//
// Accept a primitive functor for each of the patterns.
//

class PrimitivePatternVisitor : public ConstPatternVisitor
{
public:
    
    PrimitivePatternVisitor(osg::PrimitiveFunctor &functor, const TessellationHints* hints):
        _functor(functor),
        _hints(hints) {}
    
    virtual void apply(const Circle&);
    virtual void apply(const Oblong&);
    virtual void apply(const Cornet&);
    virtual void apply(const Pipe&);

    osg::PrimitiveFunctor &_functor;
    const TessellationHints *_hints;
};




void PrimitivePatternVisitor::apply(const Circle& circle)
{
    
    float tx = circle.getCenter().x();
    float ty = circle.getCenter().y();
    float tz = circle.getCenter().z();

    unsigned int numSegments = 40;
    unsigned int numRows = 20;

    float lDelta = osg::PI/(float)numRows;
    float vDelta = 1.0f/(float)numRows;

    float angleDelta = osg::PI*2.0f/(float)numSegments;
    float texCoordHorzDelta = 1.0f/(float)numSegments;

    float lBase=-osg::PI*0.5f;
    float rBase=0.0f;
    float zBase=-circle.getRadius();
    float vBase=0.0f;

    for(unsigned int rowi=0;
        rowi<numRows;
        ++rowi)
        {

            float lTop = lBase+lDelta;
            float rTop = cosf(lTop)*circle.getRadius();
            float zTop = sinf(lTop)*circle.getRadius();
            float vTop = vBase+vDelta;
            //float nzTop= sinf(lTop);
            //float nRatioTop= cosf(lTop);

            _functor.begin(GL_QUAD_STRIP);

            float angle = 0.0f;
            float texCoord = 0.0f;

            for(unsigned int topi=0;
                topi<numSegments;
                ++topi,angle+=angleDelta,texCoord+=texCoordHorzDelta)
                {

                    float c = cosf(angle);
                    float s = sinf(angle);

                    _functor.vertex(tx+c*rTop,ty+s*rTop,tz+zTop);
                    _functor.vertex(tx+c*rBase,ty+s*rBase,tz+zBase);

                }

            // do last point by hand to ensure no round off errors.
            _functor.vertex(tx+rTop,ty,tz+zTop);
            _functor.vertex(ty+rBase,ty,tz+zBase);

            _functor.end();

            lBase=lTop;
            rBase=rTop;
            zBase=zTop;
            vBase=vTop;

        }
}

void PrimitivePatternVisitor::apply(const Oblong& oblong)
{


    float x = oblong.getHalfLengths().x();
    float y = oblong.getHalfLengths().y();
    float z = oblong.getHalfLengths().z();

    Vec3 base_1(-x,-y,-z);
    Vec3 base_2(x,-y,-z);
    Vec3 base_3(x,y,-z);
    Vec3 base_4(-x,y,-z);

    Vec3 top_1(-x,-y,z);
    Vec3 top_2(x,-y,z);
    Vec3 top_3(x,y,z);
    Vec3 top_4(-x,y,z);

    if (oblong.zeroRotation()) {
        base_1 += oblong.getCenter();
        base_2 += oblong.getCenter();
        base_3 += oblong.getCenter();
        base_4 += oblong.getCenter();

        top_1 += oblong.getCenter();
        top_2 += oblong.getCenter();
        top_3 += oblong.getCenter();
        top_4 += oblong.getCenter();
    }
    else {
        Matrix matrix = oblong.getRotationMatrix();
        matrix.setTrans(oblong.getCenter());
        
        base_1 = base_1*matrix;
        base_2 = base_2*matrix;
        base_3 = base_3*matrix;
        base_4 = base_4*matrix;

        top_1 = top_1*matrix;
        top_2 = top_2*matrix;
        top_3 = top_3*matrix;
        top_4 = top_4*matrix;
    }

    _functor.begin(GL_QUADS);
    
    _functor.vertex(top_1);
    _functor.vertex(base_1);
    _functor.vertex(base_2);
    _functor.vertex(top_2);

    _functor.vertex(top_2);
    _functor.vertex(base_2);
    _functor.vertex(base_3);
    _functor.vertex(top_3);

    _functor.vertex(top_3);
    _functor.vertex(base_3);
    _functor.vertex(base_4);
    _functor.vertex(top_4);

    _functor.vertex(top_4);
    _functor.vertex(base_4);
    _functor.vertex(base_1);
    _functor.vertex(top_1);

    _functor.vertex(top_4);
    _functor.vertex(top_1);
    _functor.vertex(top_2);
    _functor.vertex(top_3);

    _functor.vertex(base_2);
    _functor.vertex(base_3);
    _functor.vertex(base_4);
    _functor.vertex(base_1);

    _functor.end();

}

void PrimitivePatternVisitor::apply(const Cornet& cornet)
{
    Matrix matrix = cornet.getRotationMatrix();
    matrix.setTrans(cornet.getCenter());


    unsigned int numSegments = 40;
    unsigned int numRows = 10;

    float r = cornet.getRadius();
    float h = cornet.getHeight();

    float normalz = r/(sqrtf(r*r+h*h));
    float normalRatio = 1.0f/(sqrtf(1.0f+normalz*normalz));
    normalz *= normalRatio;

    float angleDelta = 2.0f*osg::PI/(float)numSegments;
    float texCoordHorzDelta = 1.0/(float)numSegments;
    float texCoordRowDelta = 1.0/(float)numRows;
    float hDelta =  cornet.getHeight()/(float)numRows;
    float rDelta =  cornet.getRadius()/(float)numRows;

    float topz=cornet.getHeight()+cornet.getBaseOffset();
    float topr=0.0f;
    float topv=1.0f;
    float basez=topz-hDelta;
    float baser=rDelta;
    float basev=topv-texCoordRowDelta;
    float angle;
    float texCoord;

    for(unsigned int rowi=0;
        rowi<numRows;
        ++rowi,topz=basez, basez-=hDelta, topr=baser, baser+=rDelta, topv=basev, basev-=texCoordRowDelta)
        {
            // we can't use a fan for the cornet top
            // since we need different normals at the top
            // for each face..
            _functor.begin(GL_QUAD_STRIP);

            angle = 0.0f;
            texCoord = 0.0f;
            for(unsigned int topi=0;
                topi<numSegments;
                ++topi,angle+=angleDelta,texCoord+=texCoordHorzDelta)
                {

                    float c = cosf(angle);
                    float s = sinf(angle);

                    _functor.vertex(Vec3(c*topr,s*topr,topz)*matrix);
                    _functor.vertex(Vec3(c*baser,s*baser,basez)*matrix);

                }

            // do last point by hand to ensure no round off errors.
            _functor.vertex(Vec3(topr,0.0f,topz)*matrix);
            _functor.vertex(Vec3(baser,0.0f,basez)*matrix);

            _functor.end();

        }

    // we can't use a fan for the cornet top
    // since we need different normals at the top
    // for each face..
    _functor.begin(GL_TRIANGLE_FAN);

    angle = osg::PI*2.0f;
    texCoord = 1.0f;
    basez = cornet.getBaseOffset();

    _functor.vertex(Vec3(0.0f,0.0f,basez)*matrix);

    for(unsigned int bottomi=0;
        bottomi<numSegments;
        ++bottomi,angle-=angleDelta,texCoord-=texCoordHorzDelta)
        {

            float c = cosf(angle);
            float s = sinf(angle);

            _functor.vertex(Vec3(c*r,s*r,basez)*matrix);

        }

    _functor.vertex(Vec3(r,0.0f,basez)*matrix);
    
    _functor.end();
}

void PrimitivePatternVisitor::apply(const Pipe& pipe)
{
    Matrix matrix = pipe.getRotationMatrix();
    matrix.setTrans(pipe.getCenter());

    unsigned int numSegments = 40;

    float angleDelta = 2.0f*osg::PI/(float)numSegments;

    float texCoordDelta = 1.0/(float)numSegments;

    float r = pipe.getRadius();
    float h = pipe.getHeight();

    float basez = -h*0.5f;
    float topz = h*0.5f;

    // pipe body
    _functor.begin(GL_QUAD_STRIP);

    float angle = 0.0f;
    float texCoord = 0.0f;
    for(unsigned int bodyi=0;
        bodyi<numSegments;
        ++bodyi,angle+=angleDelta,texCoord+=texCoordDelta)
        {

            float c = cosf(angle);
            float s = sinf(angle);

            _functor.vertex(Vec3(c*r,s*r,topz)*matrix);
            _functor.vertex(Vec3(c*r,s*r,basez)*matrix);

        }

    // do last point by hand to ensure no round off errors.
    _functor.vertex(Vec3(r,0.0f,topz)*matrix);
    _functor.vertex(Vec3(r,0.0f,basez)*matrix);

    _functor.end();


    // pipe top
    _functor.begin(GL_TRIANGLE_FAN);

    _functor.vertex(Vec3(0.0f,0.0f,topz)*matrix);

    angle = 0.0f;
    texCoord = 0.0f;
    for(unsigned int topi=0;
        topi<numSegments;
        ++topi,angle+=angleDelta,texCoord+=texCoordDelta)
        {

            float c = cosf(angle);
            float s = sinf(angle);

            _functor.vertex(Vec3(c*r,s*r,topz)*matrix);

        }

    _functor.vertex(Vec3(r,0.0f,topz)*matrix);

    _functor.end();

    // pipe bottom
    _functor.begin(GL_TRIANGLE_FAN);

    _functor.vertex(Vec3(0.0f,0.0f,basez)*matrix);

    angle = osg::PI*2.0f;
    texCoord = 1.0f;
    for(unsigned int bottomi=0;
        bottomi<numSegments;
        ++bottomi,angle-=angleDelta,texCoord-=texCoordDelta)
        {

            float c = cosf(angle);
            float s = sinf(angle);

            _functor.vertex(Vec3(c*r,s*r,basez)*matrix);

        }

    _functor.vertex(Vec3(r,0.0f,basez)*matrix);

    _functor.end();
}


///////////////////////////////////////////////////////////////////////////////
//
// PatternDrawable itself..
//


PatternDrawable::PatternDrawable() : ShapeDrawable()
{
}

PatternDrawable::PatternDrawable(Shape *shape) : ShapeDrawable(shape)
{
}

PatternDrawable::PatternDrawable(const PatternDrawable& pg,const CopyOp& copyop) :
    ShapeDrawable(pg, copyop)
{
}

PatternDrawable::~PatternDrawable()
{
}

void PatternDrawable::drawImplementation(State& state) const
{
    if (_shape.valid()) {
        glColor4fv(_color.ptr());
    
        DrawPatternVisitor dsv(state,_tessellationHints.get());
        
        _shape->accept(dsv);
    }
}

void PatternDrawable::accept(ConstAttributeFunctor&) const
{
}

void PatternDrawable::accept(PrimitiveFunctor& pf) const
{
    if (_shape.valid()) {
        PrimitivePatternVisitor psv(pf,_tessellationHints.get());
        _shape->accept(psv);
    }
}


BoundingBox PatternDrawable::computeBound() const
{
    _boundingBox.init();


    if (_shape.valid()) {
        ComputeBoundPatternVisitor cbsv(_boundingBox);
        _shape->accept(cbsv);
        _boundingBoxComputed = true;
    }

    return _boundingBox;
}
