Airborne collision avoidance system and multi-angle camera recording
with living earth map and geographic position tracking. Software which
bases to 3D graphics toolkit Open Scene Graph, terrain rendering SDK
osgEarth, development library for the development of Augmented Reality
or Mixed Reality osgART and The Augmented Reality Tool Kit ARToolKit.

http://www.openscenegraph.org/
http://osgearth.org/
http://www.osgart.org/index.php/Main_Page
http://sourceforge.net/projects/artoolkit/
