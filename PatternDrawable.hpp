/* -*-c++-*- OpenSceneGraph - Copyright (C) 1998-2003 Robert Osfield 
 *
 * This library is open source and may be redistributed and/or modified under  
 * the terms of the OpenSceneGraph Public License (OSGPL) version 0.0 or 
 * (at your option) any later version.  The full license is in LICENSE file
 * included with this distribution, and on the openscenegraph.org website.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * OpenSceneGraph Public License for more details.
 */

#ifndef OSG_PATTERNDRAWABLE
#define OSG_PATTERNDRAWABLE 1

#include <osg/GL>
#include <osg/Array>
#include <osg/BoundingBox>
#include <osg/Drawable>
#include <osg/Notify>
#include <osg/PrimitiveSet>
#include <osg/ShapeDrawable>
#include <osg/Vec2>
#include <osg/Vec3>
#include <osg/Vec4>
#include "Pattern.hpp"

using namespace osg;

class PatternDrawable : public ShapeDrawable
{
public:

  PatternDrawable();

  PatternDrawable(Shape *shape);

  /** Copy constructor using CopyOp to manage deep vs shallow copy.*/
  PatternDrawable(const PatternDrawable& pg,const CopyOp& copyop=CopyOp::SHALLOW_COPY);
            
  virtual Object* cloneType() const { return new PatternDrawable(); }
  virtual Object* clone(const CopyOp& copyop) const { return new PatternDrawable(*this,copyop); }        
  virtual bool isSameKindAs(const Object* obj) const { return dynamic_cast<const PatternDrawable*>(obj)!=NULL; }
  virtual const char* libraryName() const { return "osg"; }
  virtual const char* className() const { return "PatternDrawable"; }

  /** set the color of the pattern.*/
  void setColor(const Vec4& color) { _color = color; }
        
  /** get the color of the pattern.*/
  const Vec4& getColor() const { return _color; }

  void setTessellationHints(TessellationHints* hints) { _tessellationHints = hints; }
        
  TessellationHints* getTessellationHints() { return _tessellationHints.get(); }
  const TessellationHints* getTessellationHints() const { return _tessellationHints.get(); }



  /** draw PatternDrawable directly ignoring an OpenGL display list which could be attached.
   * This is the internal draw method which does the drawing itself,
   * and is the method to override when deriving from PatternDrawable for user-drawn objects.
   */
  virtual void drawImplementation(State& state) const;

  /** return false, PatternDrawable does not support accept(AttributeFunctor&).*/
  virtual bool supports(AttributeFunctor&) const { return false; }

  /** return true, PatternDrawable does support accept(ConstAttributeFunctor&).*/
  virtual bool supports(ConstAttributeFunctor&) const { return true; }

  /** accept an ConstAttributeFunctor and call its methods to tell it about the interal attributes that this Drawable has.*/
  virtual void accept(ConstAttributeFunctor& af) const;

  /** return true, PatternDrawable does support accept(PrimitiveFunctor&) .*/
  virtual bool supports(PrimitiveFunctor&) const { return true; }

  /** accept a PrimtiveFunctor and call its methods to tell it about the interal primtives that this Drawable has.*/
  virtual void accept(PrimitiveFunctor& pf) const;

protected:

  PatternDrawable& operator = (const PatternDrawable&) { return *this;}

  virtual ~PatternDrawable();
       
  virtual BoundingBox computeBound() const;

};

#endif
